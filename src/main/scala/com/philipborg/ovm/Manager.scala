package com.philipborg.ovm

import java.security.InvalidParameterException
import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

import scala.collection.concurrent.TrieMap

private class PutTask extends Runnable {
  def run = {
    
  }
}

private class GetTask(realm:Int, x:Long, y:Long, z:Long, level:Byte) extends Callable[Voxel] {
  def call = {
    null
  }
}

private class CleanTask(realm:Int) extends Runnable {
  def run = {
    
  }
}

class Manager(db: Database) extends AutoCloseable {
  if (!path.exists) path.createDirectory;
  if (!path.isDirectory) throw new IllegalArgumentException("Path is not a directory");

  private val realms = new TrieMap[Int, ExecutorService];

  //TODO Load all current maps

  def createRealm(id: Int) = {
    if (realms.contains(id)) throw new InvalidParameterException("Realm " + id + " does already exist in list.");
    path./(id.toString).createDirectory();

    //Creates an empty map
    val emptyNode = Array.fill[Byte](16)(0);
    path./(id.toString)./("0_0_0_0").writ;

    realms.put(id, Executors.newSingleThreadExecutor());
  }

  def removeRealm(id: Int) = synchronized {
    val realm = realms.get(id).getOrElse(throw new InvalidParameterException("Id does not represent any realms."));
    realms -= id;
    realm.shutdownNow();
    val file:File = path.resolve(id.toString);
    file.delete(false);
  }

  //Put functions

  def put(realm: Int, x: Long, y: Long, z: Long, level: Byte, voxel: Voxel) = {
    
  }

  def putAllBox(realm: Int, start: (Long, Long, Long), end: (Long, Long, Long), level: Byte, voxel: Voxel) = {
    for (x <- start._1 to end._1; y <- start._2 to end._2; z <- start._3 to end._3) {
      put(realm, x, y, z, level, voxel);
    }
  }

  def putAll(voxels: (Int, Long, Long, Long, Byte, Voxel)*) = {
    voxels.foreach(v => put(v._1, v._2, v._3, v._4, v._5, v._6));
  }

  def putAll(voxels: List[(Int, Long, Long, Long, Byte, Voxel)]) = {
    voxels.foreach(v => put(v._1, v._2, v._3, v._4, v._5, v._6));
  }

  //Get functions

  def get(realm: Int, x: Long, y: Long, z:Long, level:Byte): Voxel = {
    null
  }
  
  //Clean functions
  
  def clean(realm:Int*) = {
    
  }
  
  def cleanAll = {
    realms.foreach(k => clean(k._1));
  }
  
  def clean(realms:List[Int]){
    realms.foreach { r => clean(r) }
  }

  def close() = {

  }
}